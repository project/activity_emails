CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Send emails about site activity (create and update entities) to an email 
address.

 * For a full description of the module visit:
   https://www.drupal.org/project/activity_emails

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/activity_emails


REQUIREMENTS
------------

Emails will need to be set up in the system.


INSTALLATION
------------

Install the "Activity emails" module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.



CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > Activity emails and enable
       the checkbox to make sure the emails are sent. Then fill out the email
       address to send the emails to.



MAINTAINERS
-----------

 * Fran Garcia-Linares (fjgarlin) - https://www.drupal.org/u/fjgarlin

Supporting organization:

 * AmazeeLabs - https://www.drupal.org/amazee-labs
